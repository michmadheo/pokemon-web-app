import {css} from '@emotion/css';
import '../css/mainPage.css';
import { useState } from 'react';
import pokemonLogo from '../images/pokemonLogo.png';
import menuButton from '../images/menuButton.png';
import {Link} from 'react-router-dom';
import {screenScroll} from '../functions/functions';

function Navigation() {
  const [menu, setMenu] = useState(0);

  return (
    <div>
        <div className={css`
        position: fixed;
        width: 100%;
        `}>
            {menu === 1 && 
                <div className={css`
                    display:flex;
                    position:fixed;
                    width:100%;
                    height:100%;
                `}>
                    <div className={css`
                    display: flex;
                    background-color:#1b1e24;
                    width: 70%;
                    `} >
                        <div className={css`
                        color:white;
                        font-size: 20px;
                        margin-left:10px;
                        `}>
                            <Link style={{textDecoration:'none'}} to="/">
                                <p className={css`
                                    color:white;
                                `} onClick={()=>{
                                    setMenu(0)
                                    screenScroll(false)
                                    }}>Pokemon List</p>
                            </Link>
                            <Link style={{textDecoration:'none'}} to="/mypokemon">
                                <p className={css`
                                    color:white;
                                `} onClick={()=>{
                                    setMenu(0)
                                    screenScroll(false)
                                    }}>My Pokemon</p>
                            </Link>
                        </div>
                        <div className={css`
                            position:fixed;
                            bottom:0;
                            margin-left:10px;
                            margin-bottom:10px;
                            color:gray;
                            font-size: 12px;
                        `}>
                            <a>Pokemon App by Michael Amadheo</a>
                        </div>
                    </div>
                    <div onClick={()=>{
                        setMenu(0)
                        screenScroll(false)
                    }} className={css`
                    display: flex;
                    opacity: 0.2;
                    background-color: #1b1e24;
                    width: 30%;
                    `} >
                    </div>
                </div>}
        <div className={css`
            background-color: #282c34;
            display: flex;
            min-height: 8vh;
            width: 100%;
            flex-direction: row;
            align-items: center;
            font-size: calc(10px + 2vmin);
            color: white;
            text-align: center;
        `}>
            <div onClick={()=>{
                setMenu(1)
                screenScroll(true)
            }} className={css`
                margin-left:10px;
                `}>
                <img src={menuButton} className={css`
                    height: 25px;`
                    }/>
            </div>
            <div className={css`
            width:100%;
            margin-right:35px;
            `}>
                <img src={pokemonLogo} 
                className={css`
                height: 40px;
                width: auto;
                `} 
                alt="logo" />
            </div>
        </div>
        </div>
    </div>
  );
}

export default Navigation;
