import './App.css';
import Navigation from './navigation/Navigation';
import PokemonList from './pages/PokemonList';
import MyPokemon from './pages/MyPokemon';
import PokemonDetail from './pages/pokemonDetail';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
  return (
    <Router>
      <div>
        <Navigation/>
        <Switch>
          <Route path="/" exact component={PokemonList}/>
          <Route path="/mypokemon" component={MyPokemon}/>
          <Route path="/pokemondetail/:id" component={PokemonDetail}/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
