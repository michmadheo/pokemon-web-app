import '../css/mainPage.css';
import {css} from '@emotion/css';
import React, { useState, useEffect } from 'react';
import {capitalize, screenScroll} from '../functions/functions';
import Modal from 'react-modal';
import pokeball from '../images/pokeball.png';

function MyPokemon(){
    const [myCollection, setCollection] = useState([]);
    const [modal, setModal] = useState(false);
    const [modalConfirmRelease, setModalConfirm] = useState(false);
    const [selectedPokemonName, setName] = useState("");
    const [selectedPokemonNickname, setNickname] = useState("");
    const [selectedPokemonImage, setImage] = useState("");
    const [pokemonIndex, setIndex] = useState(0);
    const [showEmpty, setShowEmpty] = useState(false);
    const [modalRelease, setModalRelease] = useState(false);


    useEffect(async ()=>{
        getMyPokemon()
    },[])

    const getMyPokemon = async() =>{
        let myPokemon = await JSON.parse(sessionStorage.getItem('mypokemon'));
        if(myPokemon === null){
            setCollection([]);
        }
        else{
            setCollection(myPokemon);
            if(myPokemon.length > 0){
                setShowEmpty(false)
            }
            else{
                setShowEmpty(true)
            }
        }
    }

    function setPokemon(name,nickname,image, index){
        screenScroll(true)
        setModal(true)
        setNickname(nickname)
        setName(name)
        setImage(image)
        setIndex(index)
    }

    const releasePokemon= async()=>{
        await myCollection.splice(pokemonIndex,1)
        await sessionStorage.setItem('mypokemon', JSON.stringify(myCollection))
        setModal(false)
        if(myCollection.length > 0){
            setShowEmpty(false)
        }
        else{
            setShowEmpty(true)
        }
    }

    return(
        <div className={css`
            align-items: center;
            margin-left: 5px;
            margin-right: 5px;
            padding-top: 80px;
        `}>
            {showEmpty &&
                <div className={css`
                    display:flex;
                    flex-direction:column;
                    justify-content:center;
                    align-items:center;
                    text-align:center;
                    font-size:20px;
                    font-weight:bold;
                `}>
                    <a>You don't have any Pokemon yet</a>
                    <img className={css`
                        height:50px;
                        width:50px;
                        margin-top:30px;
                    `} src={pokeball}/>
                </div>
            }
            <div>
                {myCollection.map((item, index)=>{
                    return(
                            <div onClick={()=>{setPokemon(item.name, item.nickname, item.imageLink, index)}} key={index} className={css`
                                        display: inline-block;
                                        flex-direction: row;
                                        height:100px;
                                        width: 30%;
                                        margin-top: 10px;
                                        margin-bottom: 50px;
                                        margin-right: 5px;
                                        margin-left: 5px;
                                        border-radius: 100px;
                                        text-align: center;
                            `}>
                                <img className="List-images" src={item.imageLink}/>
                                <div>
                                    <p className="List-name">{capitalize(item.nickname)}</p>
                                </div>
                            </div>
                    )
                })}
            </div>
            <Modal isOpen={modal}>
            <div className={css`
                    height:100%;
                    width:100%;
                    display:flex;
                    flex-direction:column;
                    justify-content:center;
                    align-items:center;
                `}>
                    <img className={css`
                        height:200px;
                    `} src={selectedPokemonImage}/>
                    <div className={css`
                        display:flex;
                        flex-direction:column;
                        text-align:center;
                    `}>
                        <a className={css`
                            font-size:30px;
                            font-weight: bold;
                        `}>{capitalize(selectedPokemonNickname)}</a>
                        <a className={css`
                            color:#636363;
                        `}>{capitalize(selectedPokemonName)}</a>
                    </div>
                    <div>
                        <button onClick={()=>{
                            setModal(false)
                            setNickname("")
                            screenScroll(false)
                            }} className={css`
                            display:flex;
                            margin-top:20px;
                            width:130px;
                            height:40px;
                            justify-content:center;
                            align-items:center;
                            border:none;
                            outline: none;
                            border-radius:50px;
                            font-weight:bold;
                            background-color:#29ab46;
                            color:white;
                        `}>Keep</button>
                        <button className={css`
                            display:flex;
                            margin-top:7px;
                            width:130px;
                            height:40px;
                            justify-content:center;
                            align-items:center;
                            background-color:#FF6F6F;
                            border:none;
                            outline: none;
                            border-radius:50px;
                            font-weight:bold;
                            color:white;
                        `} onClick={()=>{
                            setModal(false)
                            setModalConfirm(true)
                            }}>Release</button>
                    </div>
                </div>
            </Modal>
            <Modal isOpen={modalConfirmRelease}>
                <div className={css`
                    height:100%;
                    width:100%;
                    display:flex;
                    flex-direction:column;
                    justify-content:center;
                    align-items:center;
                `}>
                    <a className={css`
                            font-size:25px;
                            font-weight: bold;
                        `}>Are you sure?</a>
                    <button onClick={()=>{
                            setModalConfirm(false)
                            setModalRelease(true)
                            releasePokemon()
                            }} className={css`
                            display:flex;
                            margin-top:20px;
                            width:130px;
                            height:40px;
                            justify-content:center;
                            align-items:center;
                            background-color:#29ab46;
                            border:none;
                            outline: none;
                            border-radius:50px;
                            font-weight:bold;
                            color:white;
                        `}>Yes</button>
                        <button className={css`
                            display:flex;
                            margin-top:7px;
                            width:130px;
                            height:40px;
                            justify-content:center;
                            align-items:center;
                            background-color:#FF6F6F;
                            border:none;
                            outline: none;
                            border-radius:50px;
                            font-weight:bold;
                            color:white;

                        `} onClick={()=>{
                            setModalConfirm(false)
                            setModal(true)
                            }}>No</button>
                </div>
            </Modal>
            <Modal isOpen={modalRelease}>
                <div className={css`
                    height:100%;
                    width:100%;
                    display:flex;
                    flex-direction:column;
                    justify-content:center;
                    align-items:center;
                `}>
                    <a className={css`
                            font-size:25px;
                            font-weight: bold;
                            text-align:center;
                        `}>You've released {selectedPokemonNickname}!</a>
                        <button className={css`
                            display:flex;
                            margin-top:20px;
                            width:130px;
                            height:40px;
                            justify-content:center;
                            align-items:center;
                            background-color:#FF6F6F;
                            border:none;
                            outline: none;
                            border-radius:50px;
                            font-weight:bold;
                            color:white;
                        `} onClick={()=>{
                            screenScroll(false)
                            setModalRelease(false)
                            setNickname("")
                            setName("")
                            setImage("")
                            setIndex(0)
                            }}>Okay</button>
                </div>
            </Modal>
        </div>
    )
}

export default MyPokemon

