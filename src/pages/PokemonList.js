import '../css/mainPage.css';
import { Component} from 'react';
import {css} from '@emotion/css';
import {Link} from 'react-router-dom';
import {pokemonList} from '../api/api';
import {capitalize} from '../functions/functions';
import pokeballcolor from '../images/pokeballcolor.png';

export default class PokemonList extends Component{
    constructor(props){
        super(props)
        this.state = {
            Poke: "Pokemon List",
            pokemonList: [],
            myPokemon: 0,
            paging:0,
            load: false
        }
        this.handleScroll = this.handleScroll.bind(this);
    }

    render(){
        return(
            <div className={css`
                align-items: center;
                margin-left: 5px;
                margin-right: 5px;
                padding-top: 80px;
            `}>
                {this.state.myPokemon > 0 &&
                <Link to="/mypokemon">
                    <div className={css`
                        position:fixed;
                        display:flex;
                        right:0;
                        background-color:#282c34;
                        color:white;
                        padding-left:10px;
                        padding-right:10px;
                        margin-right:10px;
                        margin-bottom:10px;
                        justify-content:center;
                        align-items:center;
                        border-radius:100px;
                        font-size:20px;
                        padding-top:10px;
                        padding-bottom:10px;
                    `}>
                        <img className={css`
                            height:25px;
                            width:25px;
                            margin-right:5px;
                        `} src={pokeballcolor}/>
                        <a>{this.state.myPokemon}</a>
                    </div>
                </Link>
                }
                <div>
                    {this.state.pokemonList.map((item,index)=>{
                        return(
                            <Link to={`/pokemondetail/${item.id}`}>
                                <div key={index} className={css`
                                        display: inline-block;
                                        flex-direction: row;
                                        height:100px;
                                        width: 30%;
                                        margin-top: 10px;
                                        margin-bottom: 50px;
                                        margin-right: 5px;
                                        margin-left: 5px;
                                        border-radius: 100px;
                                        text-align: center;
                                `}>
                                    <img className="List-images" src={item.imageLink}/>
                                    <p className="List-name">{capitalize(item.name)}</p>
                                </div>
                            </Link>
                        )
                    })}
                </div>
                {this.state.load === true &&
                                    <div className={css`
                                    display:flex;
                                    justify-content:center;
                                `}>
                                    <a>Loading more Pokemon ...</a>
                                </div>
                }
            </div>
        )
    }

    componentDidMount(){
        this.getPokemonList()
        this.myPokemonList()
        window.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll, true);
    }

    handleScroll() {

        if ((window.scrollY + window.innerHeight) >= document.body.scrollHeight-70) {
            if(this.state.load === false && this.state.pokemonList.length >0){
                this.getMorePokemonList()
            }
        }
      }


    async myPokemonList(){
        let myPokemon = await JSON.parse(sessionStorage.getItem('mypokemon'))
        if(myPokemon === null){
            myPokemon = []
            await sessionStorage.setItem('mypokemon', JSON.stringify(myPokemon))
            this.setState({
                myPokemon: myPokemon.length
            })
        }
        else{
            this.setState({
                myPokemon: myPokemon.length
            })
        }
    }

    async getPokemonList(){
        this.setState({
            pokemonList:[]
        })
        let pokemonData = [];
        fetch(pokemonList+"?offset=0&limit=30")
          .then((response) => response.json())
          .then(async(json) => {
              pokemonData = await json.results
              let pokemonImages = await Promise.all(pokemonData.map(async(item,index)=> await Promise.resolve(
                  fetch(item.url).then((response)=>response.json())
                  .then(async(json)=>{
                      let imageLink = await json.sprites.other["official-artwork"].front_default
                      pokemonData[index].imageLink = await json.sprites.other["official-artwork"].front_default
                      pokemonData[index].id = item.url.slice(34)
                  })
              )))
                  
              this.setState({
                  Poke:json.results.length,
                  pokemonList: pokemonData,
                  paging: this.state.paging+30
              })
          })
          .catch((error) => {
          });
    }

    async getMorePokemonList(){
        this.setState({
            load: true
        })
        let pokemonData = [];
        fetch(pokemonList+"?offset="+this.state.paging+"&limit=30")
          .then((response) => response.json())
          .then(async(json) => {
              pokemonData = await json.results
              let pokemonImages = await Promise.all(pokemonData.map(async(item,index)=> await Promise.resolve(
                  fetch(item.url).then((response)=>response.json())
                  .then(async(json)=>{
                      let imageLink = await json.sprites.other["official-artwork"].front_default
                      pokemonData[index].imageLink = await json.sprites.other["official-artwork"].front_default
                      pokemonData[index].id = item.url.slice(34)
                  })
              )))
              this.setState({
                  pokemonList: this.state.pokemonList.concat(pokemonData),
                  paging: this.state.paging+30,
              },()=>{this.setState({
                        load: false
              })})
          })
          .catch((error) => {
            console.log(error)
            this.setState({
                load: false
            })
          });
    }
}

