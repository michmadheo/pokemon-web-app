import '../css/detail.css';
import {css} from '@emotion/css';
import React, { useState, useEffect } from 'react';
import {pokemonList} from '../api/api';
import {capitalize, typeColor, screenScroll} from '../functions/functions';
import Modal from 'react-modal';
import down from '../images/down.png';
import up from '../images/up.png';

function PokemonDetail({match}){
    const [errorText, setText] = useState("");
    const [myPokemon, setMyPokemon] = useState([]) 
    const [pokemonName, setName] = useState("");
    const [imageLink, setLink] = useState("");
    const [imageFront, setLinkFront] = useState("");
    const [imageBack, setLinkBack] = useState("");
    const [imageFrontShiny, setLinkFrontShiny] = useState("");
    const [imageBackShiny, setLinkBackShiny] = useState("");
    const [type, setType] = useState([]);
    const [height, setHeight] = useState(0);
    const [weight, setWeight] = useState(0);
    const [moves, setMoves] = useState([]);
    const [modal, setModal] = useState(false);
    const [modalFail, setModalFail] = useState(false);
    const [nickname, setNickname] = useState("");
    const [mainColor, setMainColor] = useState("");
    const [showMoves, setShowMoves] = useState(false);
    const [message, setMessage] = useState("");


    useEffect(()=>{
        getMyPokemon()
        getPokemonDetail()
    },[])

    const getMyPokemon = async()=>{
        let myPoke = await JSON.parse(sessionStorage.getItem('mypokemon'));
        await setMyPokemon(myPoke);
    }

    const getPokemonDetail = async () =>{
        const data = await fetch(pokemonList+`${match.params.id}`);
        const items = await data.json()
        setName(items.name);
        setLink(items.sprites.other["official-artwork"].front_default);
        setLinkFront(items.sprites.front_default)
        setLinkBack(items.sprites.back_default)
        setLinkFrontShiny(items.sprites.front_shiny)
        setLinkBackShiny(items.sprites.back_shiny)
        setType(items.types);
        setHeight(items.height/10);
        setWeight(items.weight/10);
        setMoves(items.moves);
        setMainColor(items.types[0].type.name);
    }

    const catchPokemon = async() =>{
        let probability = await (Math.random() * 2)+1
        if(probability < 2){
            setModal(true)
            screenScroll(true)
        }
        else{
            setMessage("Oops, it ran away!")
            setModalFail(true)
            screenScroll(true)
        }
    }

    const addToMyPokemon = async()=>{
        if(nickname.length > 0){
            if(nickname.length > 10){
                setText("That's too long! (10 Characters max)")
            }
            else{
                let name = true;
                for(let i=0; i<myPokemon.length ;i++){
                    if(nickname.trim().toLowerCase() === myPokemon[i].nickname.trim().toLowerCase()){
                        i = myPokemon.length;
                        name = false;
                        setText("You already give this nickname to a pokemon");
                    }
                }
                if(name === true){
                    let newPokemon = {
                        "name": pokemonName,
                        "nickname": nickname.trim(),
                        "url": pokemonList+`${match.params.id}`,
                        "imageLink": imageLink,
                        "id":`${match.params.id}`
                    }
                    await myPokemon.push(newPokemon)
                    await sessionStorage.setItem('mypokemon', JSON.stringify(myPokemon))
                    setModal(false)
                    setNickname("")
                    setModalFail(true);
                    setMessage("Pokemon added succesfully!")
                }
            }
        }
        else{
            setText("You haven't entered a nickname")
        }
    }

    const showMovesControl = () =>{
        if(showMoves){
            return moves.length
        }
        else{
            return 4;
        }
    }

    return(
        <div className="App">
            {/* <a>{data.name}</a> */}
            <div className="List-body">
                <img className="Detail-images" src={imageLink}/>
                <p className="Detail-name">{capitalize(pokemonName)}</p>
                <div className={css`
                    display:flex;
                    align-items:center;
                    justify-content:center;
                `}>
                    {type.map((item,index)=>{
                        return(
                            // <h1>{item.name}</h1>
                                <div style={{backgroundColor:typeColor(item.type.name)}} 
                                className={css`
                                    margin-right:10px;
                                    border-radius:20px;
                                    padding-top:5px;
                                    padding-bottom:5px;
                                `} key={index}>
                                    <a className={css`
                                        padding-left:20px;
                                        padding-right:20px;
                                        color: white;
                                        font-weight:400;
                                    `}>{capitalize(item.type.name)}</a>
                                </div>
                        )
                    })}
                </div>
                <img className="Detail-images-sprite" src={imageFront}/>
                <img className="Detail-images-sprite" src={imageBack}/>
                <img className="Detail-images-sprite" src={imageFrontShiny}/>
                <img className="Detail-images-sprite" src={imageBackShiny}/>
                <div className={css`
                    width:100%;
                    margin-top:10px;
                    display:flex;
                `}>
                    <div className={css`
                        width:50%
                    `}>
                        <a className={css`
                            font-weight:bold;
                            font-size:20px;
                        `}>Height</a>
                        <p>{height} m</p>
                    </div>
                    <div className={css`
                        width:50%
                    `}>
                        <a className={css`
                            font-weight:bold;
                            font-size:20px;
                        `}>Weight</a>
                        <p>{weight} kg</p>
                    </div>
                </div>
                <div>
                <a className={css`
                    font-weight:bold;
                    font-size:20px;
                `}>Moves</a>
                </div>
                <div className={css`
                width:100%
                height:20px;
                margin-top: 10px;
                margin-left:20px;
                margin-right:20px;
                `}>
                    {moves.slice(0,showMovesControl()).map((item,index)=>{
                        return(
                            <div style={{backgroundColor:typeColor(mainColor)}} className={css`
                            display:flex;
                            width:100%;
                            text-align:left;
                            margin-bottom:5px;
                            height:50px;
                            align-items: center;
                            border-radius:50px;
                            `}>
                                <a className={css`
                                    margin-left:30px;
                                    margin-right:10px;
                                    font-weight:500;
                                    color:white;
                                    font-size:15px;
                                `}>{capitalize(item.move.name.replace(/-/g, ' '))} </a>
                            </div>
                        )
                    })}
                </div>
                {moves.length > 4 &&
                    <img onClick={()=>{
                         setShowMoves(!showMoves)
                         }
                         } className={css`
                             height:30px;
                              weight:30px;
                         `} src={showMoves?up:down}/>
                }
                <div className={css`
                    margin-left:20px;
                    margin-right: 20px;
                    margin-top:20px;
                    margin-bottom:20px;
                `}>
                    <button onClick={catchPokemon} className={css`
                        width:100%;
                        height: 50px;
                        border-radius:10px;
                        outline:none;
                        border: none;
                        background-color:#FF6F6F;
                        color: white;
                        font-size: 20px;
                        font-weight: bold;
                    `}>
                        <a>Catch</a>
                    </button>
                </div>
            </div>
            <Modal isOpen={modal}>
                <div className={css`
                    height:100%;
                    width:100%;
                    display:flex;
                    flex-direction:column;
                    justify-content:center;
                    align-items:center;
                `}>
                    <a className={css`
                        font-weight:bold;
                        font-size:30px;
                    `}>Gotcha!</a>
                    <p className={css`
                        text-align: center;
                        padding-left:70px;
                        padding-right:70px;
                        font-size:18px;
                    `}>you've caught {capitalize(pokemonName)}</p>
                    <a className={css`
                        font-size:20px;
                        text-align: center;
                        margin-bottom:10px;
                    `}>Enter a nickname</a>
                    <div>
                        <input
                            className={css`
                                height:30px;
                                border:none;
                                outline:none;
                                background-color: #F0EAEA;
                                padding-left:10px;
                            `}
                            value={nickname}
                            onChange={(e)=>{
                                setNickname(e.target.value);
                                setText("");
                            }}
                        />
                    </div>
                    <div className={css`
                        display:flex;
                        font-size:11px;
                        text-align:center;
                        color:red;
                        margin-top:5px;
                    `}>
                        <a>{errorText}</a>
                    </div>
                    <div>
                        <button className={css`
                            display:flex;
                            margin-top:20px;
                            width:130px;
                            height:40px;
                            justify-content:center;
                            align-items:center;
                            background-color:#29ab46;
                            border:none;
                            outline: none;
                            border-radius:50px;
                            font-weight:bold;
                            color:white;
                        `} onClick={()=>{addToMyPokemon()}}>Keep</button>
                        <button className={css`
                            display:flex;
                            margin-top:7px;
                            width:130px;
                            height:40px;
                            justify-content:center;
                            align-items:center;
                            background-color:#FF6F6F;
                            border:none;
                            outline: none;
                            border-radius:50px;
                            font-weight:bold;
                            color:white;
                        `} onClick={()=>{
                            setModal(false);
                            setNickname("");
                            setText("");
                            screenScroll(false);
                            }}>Release</button>
                    </div>
                </div>
            </Modal>
            <Modal isOpen={modalFail}>
                <div className={css`
                    height:100%;
                    width:100%;
                    display:flex;
                    flex-direction:column;
                    justify-content:center;
                    align-items:center;
                `}>
                    <a className={css`
                            font-size:25px;
                            font-weight: bold;
                            text-align:center;
                        `}>{message}</a>
                        <button className={css`
                            display:flex;
                            margin-top:20px;
                            width:130px;
                            height:40px;
                            justify-content:center;
                            align-items:center;
                            background-color:#FF6F6F;
                            border:none;
                            outline: none;
                            border-radius:50px;
                            font-weight:bold;
                            color:white;
                        `} onClick={()=>{
                            setModalFail(false)
                            screenScroll(false)
                            setMessage("")
                            }}>Okay</button>
                </div>
            </Modal>
        </div>
    )
}

export default PokemonDetail

