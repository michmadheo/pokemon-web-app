import logo from '../logo.svg';
import '../css/mainPage.css';
import { Component } from 'react';
import pokemonLogo from '../images/pokemonLogo.png';
import {css, jsx} from '@emotion/css';

export default class MainPage extends Component{

    render(){
        return(
            <div className="App">
                <div className={css`
                    background-color: #282c34;
                    display: flex;
                    min-height: 8vh;
                    flex-direction: column;
                    align-items: center;
                    justify-content: center;
                    font-size: calc(10px + 2vmin);
                    color: white;
                `}>
                    <img src={pokemonLogo} className="List-images"/>
                </div>
                <div className="Body">

                </div>
            </div>
        )
    }
}

