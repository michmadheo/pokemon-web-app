export function capitalize(name){
    return name.charAt(0).toUpperCase() + name.slice(1);
}

export function screenScroll(condition){
    if(condition){
        document.body.style.overflow = "hidden"
    }
    else{
        document.body.style.overflow = "unset"
    }
}

export function typeColor(type){
    if(type === 'normal'){
        return "#a1a1a1"
    }
    if(type === 'fighting'){
        return "#383838"
    }
    if(type === 'flying'){
        return "#bf9a37"
    }
    if(type === 'poison'){
        return "#9143a3"
    }
    if(type === 'ground'){
        return "#7d7380"
    }
    if(type === 'rock'){
        return "#424242"
    }
    if(type === 'bug'){
        return "#779c2d"
    }
    if(type === 'ghost'){
        return "#1f1f1f"
    }
    if(type === 'steel'){
        return "#282236"
    }
    if(type === 'fire'){
        return "#e84d61"
    }
    if(type === 'water'){
        return "#1a7fc7"
    }
    if(type === 'grass'){
        return "#00a309"
    }
    if(type === 'electric'){
        return "#c3c700"
    }
    if(type === 'psychic'){
        return "#9e009e"
    }
    if(type === 'ice'){
        return "#00b3bd"
    }
    if(type === 'dragon'){
        return "#bf7300"
    }
    if(type === 'dark'){
        return "#000000"
    }
    if(type === 'fairy'){
        return "#c74a99"
    }
    if(type === 'unknown'){
        return "#000000"
    }
    if(type === 'shadow'){
        return "#000000"
    }
}